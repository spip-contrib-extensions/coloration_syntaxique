# Changelog : Coloration Syntaxique

Changelog du plugin SPIP **Coloration Syntaxique**.


## 1.1.0 - 2024-09-24

## Added

- Pipeline SPIP `exemple_de_code_source` pour personnaliser l'affichage du code source d'un exemple de code


## 1.0.0 - 2024-09-23

## Fixed

- Regex pour rétablir les codes déjà traités par la fonction `spip_balisage_code()`


## 0.9.1 - 2024-09-02

### Fixed

- Supression de `white-space: normal;` en double

## Added

- Documentation des pipelines `coloration_syntaxique_css` et `coloration_syntaxique_javascript`


## 0.9.0 - 2024-07-19

### Fixed

- Échappement des chaines de caractères JS avec `texte_script()`
- Ne pas mettre de balise dans un commentaire ! (`#CACHE` et `#HTTP_HEADER` dans `css/coloration_syntaxique.css.html`)

## Added

- Documentation du bouton pour copier/coller une portion de code


## 0.8.1 - 2024-06-25

### Fixed

- Taille de la police du bouton copier/coller sur l'espace privé
- Espacements malvenus dans les exemples de codes ayant eux même des blocs de code
- `safehtml` trop restrictif qui mange les commentaires dont on peut avoir besoin ! `propre()` est suffisant !

## Added

- Prise en charge des `#NOTES` par le raccourci `<exemple_de_code>`


## 0.8.0 - 2024-06-24

### Added

- Un bouton `Copier` pour copier/coller le contenu d'un bloc de code
- Chargement asynchrone/synchrone des feuilles de styles CSS et des fichiers javascript nécessaires à la colorisation syntaxique
- Prise en charge des traductions
- Amélioration de la coloration syntaxique `spip`
- Une page de démonstration `demo/coloration_syntaxique.html`

## Changed

- Format du flux des pipelines `coloration_syntaxique_css` et `coloration_syntaxique_javascript`


## 0.7.0 - 2024-06-20

### Added

- Prise en compte des backticks simple dans `typo()`
- Pipeline SPIP `coloration_syntaxique_css`
- Pipeline SPIP `coloration_syntaxique_javascript`
- Des boutons d'insertion de code dans le porte plume
- Squelette `inclure/coloration_syntaxique.html`
- Squelette `inclure/exemple_de_code.html`


### Changed

- Refactorisation et optimisation du code


## 0.6.0 - 2024-06-14

- Version initiale du plugin (basé sur le plugin `dsfr_coloration_syntaxique` sans dépendance au DSFR)
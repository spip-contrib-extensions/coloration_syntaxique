<?php
/**
 * Pipelines utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline porte_plume_barre_pre_charger
 * 
 * @param array $barres
 * 
 * @return array
 **/
function coloration_syntaxique_porte_plume_barre_pre_charger($barres) {
	$fonction_coloration_syntaxique_porte_plume_barre = charger_fonction('barre', 'inc/coloration_syntaxique_porte_plume');
	$raccourcis_coloration_syntaxique = $fonction_coloration_syntaxique_porte_plume_barre();
	$raccourcis_coloration_syntaxique_boutons = [];
	foreach ( $raccourcis_coloration_syntaxique as $raccourci ) {
		$raccourcis_coloration_syntaxique_boutons[] = $raccourci['barre'];
	}

	// ajoute les boutons dans les 2 barres de SPIP
	foreach (['edition','forum'] as $nom) {
		$barre = &$barres[$nom];

		if ( !empty($raccourcis_coloration_syntaxique_boutons) ) {
			$barre->ajouterPlusieursApres('cadre', $raccourcis_coloration_syntaxique_boutons);
		}
	}

	return $barres;
}

/**
 * @pipeline porte_plume_lien_classe_vers_icone
 * 
 * @param array $flux
 * 
 * @return array
 **/
function coloration_syntaxique_porte_plume_lien_classe_vers_icone($flux) {
	$fonction_coloration_syntaxique_porte_plume_barre = charger_fonction('barre', 'inc/coloration_syntaxique_porte_plume');
	$raccourcis_coloration_syntaxique = $fonction_coloration_syntaxique_porte_plume_barre();
	$raccourcis_coloration_syntaxique_icones = [];
	foreach ( $raccourcis_coloration_syntaxique as $raccourci ) {
		$raccourcis_coloration_syntaxique_icones[$raccourci['barre']['className']] = $raccourci['icone'];
	}

	if ( !empty($raccourcis_coloration_syntaxique_icones) ) {
		return array_merge($flux, $raccourcis_coloration_syntaxique_icones);
	}

	return $flux;
}
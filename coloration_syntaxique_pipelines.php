<?php
/**
 * Pipelines SPIP utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline insert_head
 * 
 * @param string $flux
 * 
 * @return string
 **/
function coloration_syntaxique_insert_head($flux) {
	include_spip('inc/filtres'); // nécessaire pour `produire_fond_statique()`

	return $flux."\n\t".'<script type="text/javascript" src="'.produire_fond_statique('javascript/coloration_syntaxique.js').'"></script>';
}

/**
 * @pipeline header_prive
 * 
 * @param string $flux
 * 
 * @return string
 **/
function coloration_syntaxique_header_prive($flux) {
	return coloration_syntaxique_insert_head($flux);
}

/**
 * @pipeline post_echappe_html_propre
 * 
 * @param string $flux
 * 
 * @return string
 **/
function coloration_syntaxique_pre_echappe_html_propre($flux) {
	// échappement du raccourci typographique `<exemple_de_code>...</exemple_de_code>`
	if ( $flux && (stripos($flux, '<exemple_de_code') !== false || stripos($flux, '</exemple_de_code') !== false) ) {
		$fonction_exemple_de_code_echapper = charger_fonction('echapper', 'inc/exemple_de_code');
		$flux = $fonction_exemple_de_code_echapper($flux);
	}

	return $flux;
}

/**
 * @pipeline post_echappe_html_propre
 * 
 * @param string $flux
 * 
 * @return string
 **/
function coloration_syntaxique_post_echappe_html_propre($flux) {
	// traitement du raccourci typographique `<exemple_de_code>...</exemple_de_code>` (qui a été précédemment échappé)
	if ( $flux && str_contains($flux, 'base64exemple_de_code') ) {
		$fonction_exemple_de_code_traiter = charger_fonction('traiter', 'inc/exemple_de_code');
		$flux = $fonction_exemple_de_code_traiter($flux);
	}

	return $flux;
}

/**
 * @pipeline pre_typo
 * 
 * @param string $flux
 * 
 * @return string
 **/
function coloration_syntaxique_pre_typo($flux) {
	/*
		Traitement des backticks simples de code qui ne passent pas par propre.
		Prise en compte uniquement des champs sur une seule ligne pour ne pas perturber le flux.
	*/
	if ( str_contains($flux, '`') && !preg_match(",\r\n?|\n,s", $flux) ) {
		if ( preg_match_all('/`(.*)`/UimsS', $flux, $codes_en_ligne_trouves, PREG_SET_ORDER) ) {
			foreach ( $codes_en_ligne_trouves as $c ) {
				$c_position = strpos($flux, $c[0]);
				$c_longueur = strlen($c[0]);

				$code = trim($c[1]);
				if ( !empty($code) ) {

					// retransforme tous les contenus qui ont été échappés
					if ( str_contains($code, 'base64') ) {
						$fonction_coloration_syntaxique_retablir_tout_depuisHtmlBase64 = charger_fonction('retablir_tout_depuisHtmlBase64', 'inc/coloration_syntaxique');
						$code = $fonction_coloration_syntaxique_retablir_tout_depuisHtmlBase64($code);
					}

					// rétablir les codes déja traités par `spip_balisage_code()`
					if ( str_contains($code, 'spip_code') ) {
						$fonction_coloration_syntaxique_retablir_spip_balisage_code = charger_fonction('retablir_spip_balisage_code', 'inc/coloration_syntaxique');
						$code = $fonction_coloration_syntaxique_retablir_spip_balisage_code($code);
					}

					// transforme en balise de code (il n'y aura pas de coloration car on ne peut pas le préciser avec un backtick simple)
					$code = recuperer_fond('inclure/coloration_syntaxique', [
						'affichage'	=> 'en_ligne',
						'code'		=> $code,
						'raccourci'	=> 'markdown',
					]);

					// provoque un échappement `TYPO` pour ne pas toucher au code
					$code = code_echappement($code, 'TYPO', false, 'span');
				}
				
				$flux = substr_replace($flux, $code, $c_position, $c_longueur);
			}
		}
	}

	return $flux;
}
# Plugin SPIP : Coloration Syntaxique

![Démonstration de coloration syntaxique du langage SPIP](demo/coloration_syntaxique_code_spip.png)

Le plugin ajoute la coloration syntaxique à tous les langages de programmation supportés par la librairie [PrismJS](https://github.com/PrismJS/).
Il inclut aussi la prise en charge des langages `spip` (pour la typographie SPIP) et `spip-squelette` (pour les squelettes SPIP).

Une unification de l'utilisation des raccourcis typographiques `<cadre>`, `<frame>`, et `<code>` est faite
pour afficher le même résultat quelque soit le raccourci utilisé.

Le raccourci `<pre>` est quant à lui échappé et sécurisé sans traitement des balises `<code>` qu'il peut contenir.

Le raccourci typographique backtick simple markdown (ex. `` `ligne de code...` ``) est maintenant traité lors de l'appel de la fonction `typo()`,
permettant ainsi d'ajouter du code sur une ligne au fil du texte dans vos titres, sous-titres et autres champs apparentés.

Le plugin facilite le copier-coller des blocs de code. En survolant un bloc de code, un bouton « Copier » s'affiche et au clic le contenu est copié dans le presse-papier.


## Compatibilité et prérequis du plugin SPIP

Le plugin est compatible avec :

 * [SPIP 4.2+](https://www.spip.net)
 * [PHP 8.1+](https://www.php.net)

Le plugin utilise le plugin [`porte_plume`](https://plugins.spip.net/porte_plume.html) pour ajouter des boutons à la barre d'édition de code informatique.


## Nouveau raccourci SPIP : `<exemple_de_code>`

![Démonstration d'un exemple de code du langage SPIP](demo/coloration_syntaxique_exemple_de_code_spip.png)

Une fois activé, le plugin permet l'utilisation du raccourci `<exemple_de_code>...</exemple_de_code>` dans
vos contenus éditoriaux. Ce raccourci permet de présenter proprement un exemple de code `html` ou `spip`
qui affichera successivement le rendu puis le code source de l'exemple.

### Paramètres

Comme pour les modèles SPIP des paramètres sont disponibles :

- langage (`spip` par défaut)
- titre (`Exemple de code : $langage$` par défaut)

### Exemples d'utilisations

**Avec les paramètres par défaut :**
```
<exemple_de_code>
	Du texte avec {de l’italique} et {{du gras}} ou un {{ {mélange des deux} }}.
</exemple_de_code>
```

**Avec un titre personnalisé :**
```
<exemple_de_code|titre=Titre personnalisé>
	Du texte avec {de l’italique} et {{du gras}} ou un {{ {mélange des deux} }}.
</exemple_de_code>
```

**Avec du code HTML :**
```
<exemple_de_code|html>
	<div class="container">
		<div><p>Lorem Elsass ipsum bredele [...]</p></div>
		<div><p>Lorem Elsass ipsum bredele [...]</p></div>
		<div><p>Lorem Elsass ipsum bredele [...]</p></div>
	</div>
</exemple_de_code>
```
NB : `<exemple_de_code|langage=html>` donne le même résultat


## 2 nouveaux squelettes SPIP pour afficher des portions de code informatique

Vous trouverez une explication plus détaillée des paramètres d'inclusions directement
dans le code source de chaque squelette.

### `inclure/coloration_syntaxique`

```
#SET{code,#CHEMIN{demo/coloration_syntaxique_code.html}|spip_file_get_contents}
<INCLURE{
	fond=inclure/coloration_syntaxique,
	langage=html,
	code=#GET{code},
}>
```

### `inclure/exemple_de_code`

```
#SET{code,#CHEMIN{demo/coloration_syntaxique_exemple_de_code.spip}|spip_file_get_contents}
<INCLURE{
	fond=inclure/exemple_de_code,
	code=#GET{code},
}>
```


## 3 nouveaux pipelines SPIP

### 2 pipelines pour vos CSS et Javascript

Pour alléger le chargement des pages, le plugin déclenche dynamiquement le chargement des CSS/Javascript nécessaire à 
la coloration syntaxique uniquement si une portion de code est présente dans la page.

Pour ajouter vos propres CSS/Javascript vous pouvez utiliser les pipelines suivant en indiquant le chemin du fichier CSS/Javascript
à charger dynamiquement :

 * `coloration_syntaxique_css`
 * `coloration_syntaxique_javascript`

```php
/**
 * @pipeline coloration_syntaxique_css
 * 
 * @param array $flux
 * 
 * @return $flux
 **/
function nomdevotreplugin_coloration_syntaxique_css($flux) {

	// exemple pour 2 feuilles de styles CSS chargées dynamiquement :

	// 1. Feuille de style CSS sous forme de squelette `css/coloration_syntaxique_nomdevotreplugin_squelette.css.html`
	$flux['css/coloration_syntaxique_nomdevotreplugin_squelette.css'] = produire_fond_statique('css/coloration_syntaxique_nomdevotreplugin_squelette.css');

	// 2. Feuille de style CSS simple `css/coloration_syntaxique_nomdevotreplugin.css`
	$flux['css/coloration_syntaxique_nomdevotreplugin.css'] = timestamp(find_in_path('css/coloration_syntaxique_nomdevotreplugin.css'));

	return $flux;
}
```

### 1 pipeline pour personnaliser l'affichage du code source d'un exemple de code

Lors de l'affichage d'un `exemple_de_code`, vous pouvez utiliser le pipeline `exemple_de_code_source`.

```php
/**
 * @pipeline exemple_de_code_source
 * 
 * @param array $flux => [
 *     'args' => [
 *         'code'              => {string}, // code source de l'exemple
 *         'langage',          => {string}, // langage de l'exemple (spip|html)
 *         'titre',            => {string}, // titre de l'exemple
 *         'nombre_de_lignes', => {int},    // nombre de lignes du code source de l'exemple
 *     ],
 *     'data' => {string}, // rendu HTML de l'affichage du code source de l'exemple
 * ]
 * 
 * @return $flux
 **/
function nomdevotreplugin_exemple_de_code_source($flux) {

	// ne pas afficher le code source de l'exemple sur l'espace privé
	if ( test_espace_prive() ) {
		return '';
	}
	else {
		$exemple = '<div style="text-align:center;">';
		$exemple .= '<p><strong>Titre :</strong> '.$flux['args']['titre'].'</p>';
		$exemple .= '<p><strong>Langage :</strong> '.$flux['args']['langage'].'</p>';
		$exemple .= '<p><strong>Nombre de lignes :</strong> '.$flux['args']['nombre_de_lignes'].'</p>';
		$exemple .= '</div>';

		$flux['data'] .= $exemple;
	}

	return $flux;
}
```
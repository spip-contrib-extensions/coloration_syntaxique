<?php
/**
 * Surcharges des fonctions de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Surcharge de la fonction `traiter_echap_code_dist` pour traiter tous 
 * les `<code>` comme des `<pre><code>` ou `<code>` à colorier (ou pas ^^)
 * 
 * @param array $regs Tableau retourné par la regex de capture :
 *  - index 0 : la capture complète du code trouvé
 *  - index 1 : le nom du raccourci/balise concerné (code|frame|cadre)
 *  - index 2 : les attributs HTML de la balise
 *  - index 3 : le contenu du code encadré par la balise
 * 
 * @param array $options Tableau d'options
 *  - `langage`
 *  - `affichage`
 * 
 * @see traiter_echap_code_dist()
 **/
function traiter_echap_code($regs, $options = []) {

	/*
		Pour le moment, ne pas réinjecter `$attributs` dans `spip_balisage_code()` comme le core SPIP
		en attendant de trouver un fix à https://git.spip.net/spip/spip/-/issues/5955
	*/
	[, $raccourci, $attributs, $code] = $regs;

	// supprimer les sauts de ligne debut/fin (mais pas les espaces => ascii art)
	$code = preg_replace("/^[\n\r]+|[\n\r]+$/s", '', $regs[3]);

	// contenu vide
	if ( empty(trim($code)) ) {
		return '';
	}

	// le langage est précisé dans les options
	$langage = !empty($options['langage']) ? trim($options['langage']) : '';

	// essaye de déterminer le language si il est précisé dans la `class` de la balise
	if ( empty($langage) ) {
		$langage = trim(extraire_attribut("<div $attributs />", 'class') ?? '');
	}

	// retransforme tous les contenus qui ont été échappés
	if ( str_contains($code, 'base64') ) {
		$fonction_coloration_syntaxique_retablir_tout_depuisHtmlBase64 = charger_fonction('retablir_tout_depuisHtmlBase64', 'inc/coloration_syntaxique');
		$code = $fonction_coloration_syntaxique_retablir_tout_depuisHtmlBase64($code);
	}

	// rétablir les codes déja traités par `spip_balisage_code()`
	if ( str_contains($code, 'spip_code') ) {
		$fonction_coloration_syntaxique_retablir_spip_balisage_code = charger_fonction('retablir_spip_balisage_code', 'inc/coloration_syntaxique');
		$code = $fonction_coloration_syntaxique_retablir_spip_balisage_code($code);
	}

	// l'affichage est précisé dans les options
	$affichage = !empty($options['affichage']) ? trim($options['affichage']) : '';
	// essaye de déterminer l'affichage si le code contient des saut de ligne
	$code_trim = trim($code);
	if ( $affichage != 'en_bloc' && $affichage != 'en_ligne' ) {
		$affichage = preg_match(",\r\n?|\n,s", $code_trim) ? 'en_bloc' : 'en_ligne';
	}
	// garder uniquement le code sans espaces debut/fin si l'affichage est `en_ligne`
	if ( $affichage == 'en_ligne' ) {
		$code = $code_trim;
	}

	// remplacement du contenu
	$code = recuperer_fond('inclure/coloration_syntaxique', [
		'langage'	=> $langage,
		'affichage'	=> $affichage,
		'code'		=> $code,
		'raccourci'	=> $raccourci,
	]);

	// réintègre les échappements de la fonction `spip_balisage_code()`
	if ( !empty($echappements_spip_balisage_code) ) {
		$code = str_replace($echappements_spip_balisage_code['echappements'], $echappements_spip_balisage_code['contenus'], $code);
	}

	return $code;
}

/**
 * Surcharge de la fonction `traiter_echap_cadre_dist` pour traiter tous 
 * les `<cadre>` comme des `<code>` affichés sous forme de blocs
 * 
 * @see traiter_echap_cadre_dist()
 **/
function traiter_echap_cadre($regs, $options = []) {
	return traiter_echap_code($regs, array_merge($options,['affichage' => 'en_bloc']));
}

/**
 * Surcharge de la fonction `traiter_echap_frame_dist` pour traiter tous 
 * les `<frame>` comme des `<code>` affichés sous forme de blocs
 * 
 * @see traiter_echap_frame_dist()
 **/
function traiter_echap_frame($regs, $options = []) {
	return traiter_echap_code($regs, array_merge($options,['affichage' => 'en_bloc']));
}

/**
 * Ne pas faire de traitement des balises `<code>` contenu dans les `<pre>`.
 * Encode les entités HTML spéciales en caractères pour corriger la faille https://git.spip.net/spip/spip/-/issues/5955
 * 
 * @see traiter_echap_pre_dist()
 **/
function traiter_echap_pre($regs, $options = []) {
	return "\n".'<pre>'.trim(entites_html($regs[3])).'</pre>';
}
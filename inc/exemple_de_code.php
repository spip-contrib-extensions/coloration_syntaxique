<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Échappement du raccourci typographique `<exemple_de_code>...</exemple_de_code>`
 * 
 * @param string $texte
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function inc_exemple_de_code_echapper_dist($texte) {

	if ( preg_match_all('/<exemple_de_code\s*([|]*(?:<[^<>]*>|[^>])*?)*\s*>(.*)<\/exemple_de_code>/UimsS', $texte, $raccourcis_trouves, PREG_SET_ORDER) ) {
		foreach ( $raccourcis_trouves as $r ) {
			$r_position = strpos($texte, $r[0]);
			$r_longueur = strlen($r[0]);
			$r_echappe = code_echappement($r[0], 'exemple_de_code', true, 'div');

			// remplacement du raccourci par un contenu échappé
			$texte = substr_replace($texte, $r_echappe, $r_position, $r_longueur);
		}
	}

	return $texte;
}

/**
 * Traitement du raccourci typographique `<exemple_de_code>...</exemple_de_code>` qui permet 
 * d'afficher un exemple de code HTML ou SPIP (aperçu propre + code échappé)
 * 
 * Inspiré des modèles SPIP, voir `$preg_modele`, `inclure_modele()` et `creer_contexte_de_modele()`
 * 
 * Les paramètres disponibles du raccourci sont :
 *  - langage (html|spip)
 *  - titre
 * 
 * Tous les paramètres sont automatiquement ajoutés dans des attributs HTML `data-{cle_du_parametre}="{valeur_du_parametre}"`,
 * ce qui permet potentiellement de styliser avec du CSS un exemple spécifique (utilisé par défaut pour le `titre`)
 * 
 * @param string $texte
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function inc_exemple_de_code_traiter_dist($texte) {

	// retransforme les raccourcis échappés
	$texte = echappe_retour($texte, 'exemple_de_code');

	if ( preg_match_all('/<exemple_de_code\s*([|]*(?:<[^<>]*>|[^>])*?)*\s*>(.*)<\/exemple_de_code>/UimsS', $texte, $raccourcis_trouves, PREG_SET_ORDER) ) {
		foreach ( $raccourcis_trouves as $r ) {
			$r_position = strpos($texte, $r[0]);
			$r_longueur = strlen($r[0]);
			// contenu : supprimer les sauts de ligne debut/fin (mais pas les espaces => ascii art)
			$r_contenu = preg_replace("/^[\n\r]+|[\n\r]+$/s", '', $r[2]);
			$r_contenu_trim = trim($r_contenu);

			// contenu vide
			if ( empty($r_contenu_trim) ) {
				$texte = substr_replace($texte, '', $r_position, $r_longueur);
				continue;
			}

			// contenu sur une seule ligne, garder uniquement le code sans espaces debut/fin
			if ( !preg_match(",\r\n?|\n,s", $r_contenu_trim) ) {
				$r_contenu = $r_contenu_trim;
			}

			// contexte
			$exemple_de_code = [
				'raccourci'					=> true,
				'langage'					=> null,
				'titre'						=> null,
				'code'						=> $r_contenu,
			];

			// paramètres : trim et supprime les paramètres vides
			$r_params = array_filter(array_map('trim',explode('|', $r[1])));
			if ( !empty($r_params) ) {
				// essaye de déterminer le langage du code
				$argument_1 = strtolower(trim(current($r_params)));
				if ( preg_match('/^[a-z0-9_\-]+$/', $argument_1) ) { // langage valide ?
					$exemple_de_code['langage'] = $argument_1;
					array_shift($r_params); // enlever l'argument des params
				}

				// création d'un tableau d'argument comme SPIP le fait pour les modèles
				$r_params = creer_contexte_de_modele($r_params);
			}

			// le langage n'était pas dans le 1er argument, peut-être qu'il est précisé avec `langage=`
			if ( !$exemple_de_code['langage'] && array_key_exists('langage', $r_params) ) {
				$exemple_de_code['langage'] = strtolower(trim($r_params['langage']));
			}

			// un titre ?
			if ( array_key_exists('titre', $r_params) ) {
				$exemple_de_code['titre'] = $r_params['titre'];
			}

			// ajoute les paramètres au contexte
			$exemple_de_code = array_merge($r_params,$exemple_de_code);

			// remplacement du contenu
			$texte = substr_replace($texte, recuperer_fond('inclure/exemple_de_code', $exemple_de_code), $r_position, $r_longueur);
		}
	}

	return $texte;
}
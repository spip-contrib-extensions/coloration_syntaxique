<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Retourne un tableau de la liste des feuilles de styles CSS à inclure pour la coloration syntaxique
 * des blocs de code ayant la balise `<code>`.
 * 
 * @return array
 **/
function inc_coloration_syntaxique_css_dist() {
	include_spip('inc/filtres'); // nécessaire pour `timestamp()` et `produire_fond_statique()`

	// tableau des feuilles de styles CSS
	// clé = fichier, valeur = chemin complet
	$css = [];

	// styles par défaut des plugin PrismJS
	$css['lib/prismjs/plugins/toolbar/prism-toolbar.css'] = timestamp(find_in_path('lib/prismjs/plugins/toolbar/prism-toolbar.css'));

	// styles de base du plugin pour la coloration syntaxique
	$css['css/coloration_syntaxique.css'] = produire_fond_statique('css/coloration_syntaxique.css');

	// pipeline permettant d'inclure/modifier les feuilles de styles
	$css = pipeline('coloration_syntaxique_css', $css);

	return $css;
}

/**
 * Retourne un tableau de la liste des fichiers javascript à inclure pour la coloration syntaxique
 * des blocs de code ayant la balise `<code>`.
 * 
 * @return array
 **/
function inc_coloration_syntaxique_javascript_dist() {
	include_spip('inc/filtres'); // nécessaire pour `timestamp()` et `produire_fond_statique()`

	// tableau des fichiers javascript
	// clé = fichier, valeur = chemin complet
	$javascript = [];

	// PrismJS core
	$javascript['lib/prismjs/components/prism-core.min.js'] = timestamp(find_in_path('lib/prismjs/components/prism-core.min.js'));

	// PrismJS toolbar et bouton copier
	$javascript['lib/prismjs/plugins/toolbar/prism-toolbar.min.js'] = timestamp(find_in_path('lib/prismjs/plugins/toolbar/prism-toolbar.min.js'));
	$javascript['lib/prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard.min.js'] = timestamp(find_in_path('lib/prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard.min.js'));

	// préchargement des grammaires PrismJS spécifiques ou surchargées avant l'autoloader
	// c'est ici qu'on précharge la grammaire `spip` et `spip-squelette`
	$prismjs_components_perso = [];
	foreach ( find_all_in_path('javascript/', 'prismjs-component-[a-z-_]+(.min)?\.js$') as $nom => $chemin ) {
		$cle = substr($nom, 0, -3); // enlève `.js`

		// version minifiée prioritaire
		if ( substr($cle, -4) == '.min' ) {
			$cle = substr($cle, 0, -4); // enlève `.min`
			$prismjs_components_perso[$cle] = $chemin;
		}
		else if ( !array_key_exists($cle, $prismjs_components_perso) ) {
			$prismjs_components_perso[$cle] = $chemin;
		}
	}
	if ( !empty($prismjs_components_perso) ) {
		ksort($prismjs_components_perso);
		foreach ( $prismjs_components_perso as $cle => $chemin ) {
			$javascript['javascript/'.$cle.'.js'] = timestamp($chemin);
		}
	}

	// PrismJS autoloader des grammaires de chaque langage situées dans le dossier `lib/prismejs/components/` de la librairie
	$javascript['lib/prismjs/plugins/autoloader/prism-autoloader.min.js'] = timestamp(find_in_path('lib/prismjs/plugins/autoloader/prism-autoloader.min.js'));

	// pipeline permettant d'inclure/modifier les fichiers javascript
	$javascript = pipeline('coloration_syntaxique_javascript', $javascript);

	return $javascript;
}

/**
 * Rétablir TOUS les contenus échappés, quelques soit la `$source` de `code_echappement()`,
 * qui ont été échappés dans un texte en <(div|span) class="base64..."></(div|span)>
 * 
 * @see `code_echappement()`
 * @see `echappe_retour()`
 * 
 * @param string $texte
 * 
 * @return string
 **/
function inc_coloration_syntaxique_retablir_tout_depuisHtmlBase64_dist($texte) {
	if ( $texte && str_contains($texte, 'base64') ) {
		include_spip('inc/texte_mini');

		// retransforme les bouts de texte échappés sans source spécifique
		$texte = echappe_retour($texte);

		// retransforme les bouts de texte échappés avec une source spécifique
		if ( preg_match_all('/<(span|div)\sclass=[\'"]base64([^"\']*)[\'"]\s[^>]*>\s*<\/\1>/UimsS', $texte, $textes_echappes_trouves, PREG_SET_ORDER) ) {
			$sources_textes_echappes = [];
			foreach ( $textes_echappes_trouves as $texte_echappe ) {
				$sources_textes_echappes[] = $texte_echappe[2];
			}
			$sources_textes_echappes = array_unique($sources_textes_echappes);
			foreach ( $sources_textes_echappes as $source_texte ) {
				$texte = echappe_retour($texte, $source_texte);
			}
		}
	}

	return $texte;
}

/**
 * Rétablir les codes déjà traités par la fonction `spip_balisage_code()`
 * 
 * @param string $texte
 * 
 * @return string
 **/
function inc_coloration_syntaxique_retablir_spip_balisage_code_dist($texte) {
	// pas de code déjà traités
	if ( !$texte || !str_contains($texte, 'spip_code') ) {
		$texte;
	}

	/*
		Codes en bloc générés par `spip_balisage_code()`

		- index 0 : la capture complète du code trouvé
		- index 1 : les attributs HTML de la balise `<pre>`
		- index 2 : le contenu du code encadré par la balise `<code>`
	*/
	if ( preg_match_all('/<div class="precode"><pre([^>]*class=["\'][^"\']*\bspip_code\b[^"\']*["\'][^>]*)><code>(.*)<\/code><\/pre><\/div>/UimsS', $texte, $codes_en_bloc_trouves, PREG_SET_ORDER) ) {
		foreach ( $codes_en_bloc_trouves as $bloc ) {
			$bloc_position = strpos($texte, $bloc[0]);
			$bloc_attributs = trim($bloc[1]);
			$bloc_langage = trim(extraire_attribut("<div $bloc_attributs />", 'data-language') ?? '');
			$bloc_raccourci = trim(extraire_attribut("<div $bloc_attributs />", 'data-raccourci') ?? '');
			// supprimer les sauts de ligne debut/fin (mais pas les espaces => ascii art)
			$bloc_contenu = preg_replace("/^[\n\r]+|[\n\r]+$/s", '', $bloc[2]);
			// reconvertir les entités HTML spéciales en caractères
			$bloc_contenu = htmlspecialchars_decode($bloc_contenu, ENT_COMPAT | ENT_HTML401);

			// par défaut on considère que le racccourci d'origine est du markdown (transformé automatiquement par textwheel)
			$bloc_raccourci_ouvrir = "```$bloc_langage";
			$bloc_raccourci_fermer = "```";
			
			// le code provient d'un raccourci typographique SPIP autre que markdown
			if ( !empty($bloc_raccourci) && $bloc_raccourci != 'markdown' ) {
				$bloc_raccourci_class = !empty($bloc_langage) ? ' class="'.attribut_html($bloc_langage).'"' : '';
			
				$bloc_raccourci_ouvrir = '<'.$bloc_raccourci.$bloc_raccourci_class.'>';
				$bloc_raccourci_fermer = '</'.$bloc_raccourci.'>';
			}

			// contenu du raccourci avant traitement
			$bloc_raccourci_contenu = $bloc_raccourci_ouvrir."\n".$bloc_contenu."\n".$bloc_raccourci_fermer;

			// remplace
			$texte = substr_replace($texte, $bloc_raccourci_contenu, $bloc_position, strlen($bloc[0]));
		}
	}

	/*
		Codes en ligne générés par `spip_balisage_code()`

		- index 0 : la capture complète du code trouvé
		- index 1 : les attributs HTML de la balise `<code>`
		- index 2 : le contenu du code encadré par la balise `<code>`
	*/
	if ( str_contains($texte, 'spip_code') && preg_match_all('/<code([^>]*class=["\'][^"\']*\bspip_code\b[^"\']*["\'][^>]*)>(.*)<\/code>/UimsS', $texte, $codes_en_ligne_trouves, PREG_SET_ORDER) ) {
		foreach ( $codes_en_ligne_trouves as $code ) {
			$code_position = strpos($texte, $code[0]);
			$code_attributs = trim($code[1]);
			$code_langage = trim(extraire_attribut("<div $code_attributs />", 'data-language') ?? '');
			$code_raccourci = trim(extraire_attribut("<div $code_attributs />", 'data-raccourci') ?? '');
			// supprimer les espaces debut/fin
			$code_contenu = trim($code[2]);
			// reconvertir les entités HTML spéciales en caractères
			$code_contenu = htmlspecialchars_decode($code_contenu, ENT_COMPAT | ENT_HTML401);

			// par défaut on considère que le racccourci d'origine est du markdown (transformé automatiquement par textwheel)
			$code_raccourci_ouvrir = "`";
			$code_raccourci_fermer = "`";

			/*
				Forcer le raccourci `<code>` si il y a un code de langue et :
				 - pas de raccourci de précisé
				 - ou `markdown` car impossible de préciser la langue en markdown sur un code en ligne
			*/
			if (
				!empty($code_langage) &&
				(empty($code_raccourci) || $code_raccourci == 'markdown') 
			) {
				$code_raccourci = 'code'; // raccourci par défaut de SPIP
			}

			// le code provient d'un raccourci typographique SPIP autre que markdown
			if ( !empty($code_raccourci) && $code_raccourci != 'markdown' ) {
				$code_raccourci_class = !empty($code_langage) ? ' class="'.attribut_html($code_langage).'"' : '';
			
				$code_raccourci_ouvrir = '<'.$code_raccourci.$code_raccourci_class.'>';
				$code_raccourci_fermer = '</'.$code_raccourci.'>';
			}
		
			// contenu du raccourci avant traitement
			$code_raccourci_contenu = $code_raccourci_ouvrir.$code_contenu.$code_raccourci_fermer;

			// remplace
			$texte = substr_replace($texte, $code_raccourci_contenu, $code_position, strlen($code[0]));
		}
	}

	return $texte;
}
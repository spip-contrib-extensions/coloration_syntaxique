<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Liste des raccourcis typographiques de coloration syntaxique
 * à ajouter à la barre du porte plume
 *  
 * @return array $raccourcis
 **/
function inc_coloration_syntaxique_porte_plume_barre_dist() {
	// déjà en cache ?
	if ( isset($GLOBALS['coloration_syntaxique_porte_plume_barre']) ) {
		return $GLOBALS['coloration_syntaxique_porte_plume_barre'];
	}

	$raccourcis = [];
	foreach ( find_all_in_path('icones_barre/', 'coloration_syntaxique_code_[a-z-]+\.svg$') as $nom => $chemin ) {
		$icone = basename($chemin);
		$langage = str_replace(['coloration_syntaxique_code_','.svg'], '', $icone);
		
		$porte_plume = [
			'icone'		=> $icone,
			'barre'		=> [
				'id'          => 'outil_coloration_syntaxique_code_'.$langage,
				'name'        => _T('coloration_syntaxique:inserer_un_code_preformate', ['nom_du_langage' => strtoupper($langage)]),
				'className'   => 'outil_coloration_syntaxique_code_'.$langage,
				'openWith' => "```$langage\n",
				'closeWith' => "\n```",
				'display'     => true,
			],
		];

		$raccourcis['code_'.$langage] = $porte_plume;
	}

	ksort($raccourcis);

	// ajouter le bouton du raccourci `<exemple_de_code>` à la fin
	$raccourcis['exemple_de_code'] = [
		'icone'		=> 'exemple_de_code.svg',
		'barre'		=> [
			'id'          => 'outil_exemple_de_code',
			'name'        => _T('coloration_syntaxique:inserer_un_exemple_de_code', ['raccourci' => '<exemple_de_code>']),
			'className'   => 'outil_exemple_de_code',
			'openWith' => "<exemple_de_code>\n",
			'closeWith' => "\n</exemple_de_code>",
			'display'     => true,
		],
	];

	// stockage en $GLOBALS pour éviter de re-charger à chaque fois si il y a plusieurs appels à cette fonction
	$GLOBALS['coloration_syntaxique_porte_plume_barre'] = $raccourcis;

	return $raccourcis;
}
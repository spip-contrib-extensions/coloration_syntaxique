<?php
/**
 * Fonctions du plugin chargées au calcul des squelettes
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Appel de la fonction surchargeable `inc_coloration_syntaxique_css_dist()`
 * 
 * @return array
 **/
function coloration_syntaxique_css() {
	$fonction_coloration_syntaxique_css = charger_fonction('css', 'inc/coloration_syntaxique');

	return $fonction_coloration_syntaxique_css();
}

/**
 * Appel de la fonction surchargeable `inc_coloration_syntaxique_js_dist()`
 * 
 * @return array
 **/
function coloration_syntaxique_javascript() {
	$fonction_coloration_syntaxique_javascript = charger_fonction('javascript', 'inc/coloration_syntaxique');

	return $fonction_coloration_syntaxique_javascript();
}
/**
 * Coloration syntaxique de la typographie SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * @credit		marcimat, bricebou
 * 
 * Inspiré du plugin SPIP `prism`
 * https://git.spip.net/spip-contrib-extensions/prism/-/blob/master/js/prism-spip-typo.js
 * 
 * Pour le moment il manque la coloration des exemples suivants car c'est complexe à gérer :
 * 
 * - {{ {{{ Intertitre encadré de gras }}} }}
 * - { {{{ Intertitre encadré d'italique }}} }
 * - {{ [Lien encadré de gras->article1] }}
 * - { [Lien encadré d'italique->article1] }
 **/
(function () {

	if ( typeof Prism === 'undefined' || typeof document === 'undefined' ) {
		return
	}

	Prism.languages['spip'] = {
		'comment': {
			pattern: /<!--(?:(?!<!--)[\s\S])*?-->/,
			greedy: true
		},
		'spip-html': {
			pattern: /<html(?:(?!<html)[\s\S])*?<\/html>/i,
			alias: 'code',
		},
		'spip-code': [
			{
				pattern: /```[\s\S]*?```/,
				alias: 'code',
				greedy: true,
			},
			{
				pattern: /<code[\s\S]*?<\/code>/i,
				alias: 'code',
				greedy: true,
			},
		],
		'spip-cadre': {
			pattern: /<cadre[\s\S]*?<\/cadre>/i,
			alias: 'code',
			greedy: true,
		},
		'spip-frame': {
			pattern: /<frame[\s\S]*?<\/frame>/i,
			alias: 'code',
			greedy: true,
		},
		'spip-pre': {
			pattern: /<pre[\s\S]*?<\/pre>/i,
			alias: 'code',
			greedy: true,
		},
		'spip-javascript': {
			pattern: /<script[\s\S]*?<\/script>/i,
			alias: 'code',
			greedy: true,
		},
		'spip-exemple-de-code': {
			pattern: /<exemple_de_code[\s\S]*?<\/exemple_de_code>/i,
			alias: 'code',
			greedy: true,
		},
		'spip-tableau': {
			pattern: /\|\|?[\s\S]*\|?\|/,
			inside: {
				'caption': {
					pattern: /^\|\|.*?\|\|/,
					alias: 'bold',
					inside: {
						'pipe': {
							pattern: /\|\|/,
							alias: 'tag',
						},
						'resume': {
							pattern: /\|.*?$/,
							inside: {
								'pipe': {
									pattern: /\|/,
									alias: 'tag',
								},
							}
						},
					}
				},
				'pipe': {
					pattern: /\|/,
					alias: 'tag',
				},
				'fusion': {
					pattern: /^(<|\^)$/,
					alias: 'tag bold',
				},
			}
		},
		'spip-separateur': {
			pattern: /\n(----+|____+)/,
			alias: 'tag',
		},
		'spip-note' : {
			pattern: /\[\[.*?\]\]/,
			inside: {
				'punctuation': {
					pattern: /^\[\[|\]\]$/,
				},
				'url': {
					pattern: /^<.*?>/,
				}
			}
		},
		'spip-titre' : {
			pattern: /{{{.*?}}}/,
			alias: 'important',
			greedy: true,
			inside: {
				'punctuation': {
					pattern: /^{{{(#+|\*+)*|}}}$/,
					inside: {
						'parameter': /#|\*/,
					},
				},
			}
		},
		'spip-lien': {
			pattern: /\[.*?(?:->|<-|\?).*?\]/,
			alias: 'url',
			inside: {
				'punctuation': {
					pattern: /^\[|\]$/,
				},
				'titre': {
					pattern: /^.+(?:->)/,
					alias: 'attr-name',
					inside: {
					 	'operator': /(?:->)/,
					},
				},
				'operator': [
					{
						pattern: /(?:->|<-)/,
					},
					{
						pattern: /^\?/,
					}
				]
			}
		},
		'spip-italique-gras': {
			pattern: /{[^}]*?{{.*?}}.*?}/,
			alias: 'italic',
			inside: {
				'punctuation': {
					pattern: /^{|}$/,
				},
			},
		},
		'spip-gras': {
			pattern: /{{.*?}}/,
			alias: 'bold',
			inside: {
				'punctuation': {
					pattern: /^{{|}}$/,
				},
			},
		},
		'spip-italique': {
			pattern: /{.*?}/,
			alias: 'italic',
			inside: {
				'punctuation': {
					pattern: /^{|}$/,
				},
			},
		},
		'spip-balise': {
			pattern: /<\/?(quote|poesie|poetry|math|intro|multi)>/i,
			alias: 'tag',
			inside: {
				'punctuation': {
					pattern: /^<\/?|>$/,
				},
			}
		},
		'spip-modele': {
			pattern: /<([a-z_-]{3,})\s*([0-9]+|\|)+(?:<[^<>]*>|[^>])*?\s*>/,
			greedy: true,
			alias: 'tag',
			inside: {
				'punctuation': {
					pattern: /^<|>$/,
				},
				'inclure': {
					pattern: /^[^|]+/,
					inside: {
						'nom': {
							pattern: /^[a-z_-]+/,
						},
						'numero': {
							pattern: /\b\d+\b/,
							alias: 'number',
						},						
					}
				},
				'argument': {
					pattern: /^\|[^|]+/,
					inside: {
						'pipeline': {
							pattern: /\|/,
							alias: 'operator',
						},
						'valeur': {
							pattern: /=\s*(?:"[^"]*"|'[^']*'|[^|]+)/,
							alias: 'attr-value',
							inside: {
								'punctuation': [
									{
										pattern: /^=/,
										alias: 'attr-equals'
									},
									{
										pattern: /^(\s*)["']|["']$/,
										lookbehind: true
									}
								],
								'number': {
									pattern: /^\s*\d+\s*$/,
								}
							}
						},
						'parametre': {
							pattern: /\b\w+\b/,
							alias: 'parameter',
						},
					},
				},
			},
		},
		'spip-liste': {
			pattern: /-(\*|#)+/,
			alias: 'punctuation',
			inside: {
				'parameter': /#|\*/,
			},
		},
		'spip-alinea': {
			pattern: /(\n|^|\s)--\s/,
			alias: 'punctuation',
		},
		// ne pas mettre `spip-puce` car c'est une class CSS déjà utilisée par SPIP
		'spip-puce-token': {
			pattern: /(\n|^)-\s/,
			alias: 'punctuation',
		},
		'spip-manualbr': {
			pattern: /(\n|^)_\s/,
			alias: 'punctuation',
		},
		// `code` ou ``code`` depuis prism-markdown.js
		'spip-code-snippet': {
			pattern: /(^|[^\\`])(?:``[^`\r\n]+(?:`[^`\r\n]+)*``(?!`)|`[^`\r\n]+`(?!`))/,
			lookbehind: true,
			greedy: true,
			alias: 'code',
		},
		// depuis https://stackoverflow.com/a/17773849
		'url': /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/,
		'spip-espace-insecable': {
			pattern: /\\?~/,
			inside: {
				'echappe': {
					pattern: /^\\~$/,
					inside: {
						'comment': /\\/,
					},
				},
				'espace': /^~$/,
			},
		},
	}

	Prism.languages['spip']['spip-tableau'].inside.rest = Prism.languages['spip']
	Prism.languages['spip']['spip-titre'].inside.rest = Prism.languages['spip']
	Prism.languages['spip']['spip-lien'].inside['titre'].inside.rest = Prism.languages['spip']
	Prism.languages['spip']['spip-italique-gras'].inside['spip-gras'] = Prism.languages['spip']['spip-gras']
	Prism.languages['spip']['spip-gras'].inside.rest = Prism.languages['spip']
	Prism.languages['spip']['spip-italique'].inside.rest = Prism.languages['spip']
	Prism.languages['spip']['spip-modele'].inside['argument'].inside['valeur'].inside.rest = Prism.languages['spip']
}())
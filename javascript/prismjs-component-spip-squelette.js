/**
 * Coloration syntaxique des squelettes SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * @credit		bricebou
 * 
 * Inspiré de la contrib SPIP ` highlightjs-spip`
 * https://git.spip.net/spip-contrib-outils/highlightjs-spip/-/blob/main/src/languages/spip.js
 **/
(function () {

	if ( typeof Prism === 'undefined' || typeof document === 'undefined' ) {
		return
	}

	const SPIP_arguments_importants = /^(fond|doublons|env|self|ajax)=*$/
	const SPIP_operator = /!==|!=|<=|<|===|==|>=|>|=|\?|\!/
	const SPIP_punctuation = /[{}\[\](),]/
	const SPIP_parameter = {
		pattern: /[a-zA-Z0-9_]+\s*[!=<>]+/,
		inside: {
			'spip-argument-important': {
				pattern: SPIP_arguments_importants,
				inside: {
					'operator': SPIP_operator,
				},
			},
			'operator': SPIP_operator,
		}
	}

	Prism.languages['spip-squelette'] = {
		'spip-commentaire': {
			pattern: /\[\(#REM\)(?:(?!\[\(#REM\))[\s\S])*?\]/,
			greedy: true,
			alias: 'comment',
		},
		'comment': {
			pattern: /<!--(?:(?!<!--)[\s\S])*?-->/,
			greedy: true,
		},
		'spip-boucle': {
			pattern: /<\/?\/?BB?(OUCLE)?(\w*)?(?:(<|>)=?|{[^{}]*}|<[^<>]*>|[^>])*?\s*>/,
			alias: 'selector',
			inside: {
				'tag': {
					pattern: /^<[^\s(]+|>$/,
					inside: {
						'punctuation': {
							pattern: /^<|>$/,
						}
					}
				},
				'type-boucle': {
					pattern: /\([A-Z]+\)/,
					alias: 'important',
					inside: {
						'punctuation': {
							pattern: /^\(|\)$/,
						},
					}
				},
				'spip-parametres': {
					pattern: /[\s\S]+/,
					alias: 'selector',
					inside: {
						'parameter': SPIP_parameter
					},
				}
			}
		},
		'spip-inclure': {
			pattern: /<INCLU[DR]E(?:<[^<>]*>|[^>])*?\s*>/,
			inside: {
				'tag': {
					pattern: /^<[^\s{]+|>$/,
					inside: {
						'punctuation': {
							pattern: /^<|>$/,
						}
					}
				},
				'spip-parametres': {
					pattern: /[\s\S]+/,
					alias: 'selector',
					inside: {
						'parameter': SPIP_parameter
					},
				}
			}
		},
		'spip-idiome': {
			pattern: /<:(([a-z0-9_]+):)?([a-z0-9_]*)({([^\|=>]*=[^\|>]*)})?((\|[^>]*)?:?>)/,
			inside: {
				'tag': {
					pattern: /^<:[^\s:\|]+|:>$|\:/,
					inside: {
						'punctuation': {
							pattern: /^<:|:>$|\:/,
						}
					}
				},
				'spip-parametres': {
					pattern: /[\s\S]+/,
					alias: 'selector',
					inside: {
						'parameter': SPIP_parameter
					},
				}
			}
		},
		'spip-_n-balise': {
			pattern: /#_[A-Za-z]+:[A-Z_]+[A-Z_0-9]*/,
			alias: 'variable',
		},
		'spip-balise': {
			pattern: /#[A-Z_]+[A-Z_0-9]*\*{0,2}/,
			alias: 'variable',
			inside: {
				'spip-eval': {
					pattern: /^#EVAL\*{0,2}/,
				},
				'spip-const': {
					pattern: /^#CONST\*{0,2}/,
				},
				'spip-env': {
					pattern: /^#ENV\*{0,2}/,
				},
				'spip-set': {
					pattern: /^#SET\*{0,2}/,
				},
				'spip-get': {
					pattern: /^#GET\*{0,2}/,
				},
			}
		},
		'spip-filtre': {
			pattern: /\|[a-zA-Z0-9_!=<>\?]+/,
			alias: 'function',
			inside: {
				'pipeline': {
					pattern: /\|/,
					alias: 'operator',
				},
				'test': {
					pattern: /^(\?|sinon|non|oui|xou|ou|et|yes|not|and|xor|or)/,
					alias: 'operator',
				},
				'comparaison': {
					pattern: SPIP_operator,
					alias: 'operator',
				},
			}
		},
		'spip-argument': {
			pattern: /{[^{}]*}|{[^,]*,/,
			alias: 'selector',
			inside: {
				'punctuation': {
					pattern: /^{|}|,$/,
				},
				'parameter': SPIP_parameter,
				'spip-argument-important': SPIP_arguments_importants,
			}
		},
		'operator': SPIP_operator,
		'punctuation': SPIP_punctuation,
		'number': {
			pattern: /^\s*[0-9]+[0-9.*/+-]*\s*$/,
			inside: {
				'operator': {
					pattern: /[*/+-]/,
				}
			}
		},
	}

	Prism.languages['spip-squelette']['spip-boucle'].inside['spip-parametres'].inside.rest = Prism.languages['spip-squelette']
	Prism.languages['spip-squelette']['spip-inclure'].inside['spip-parametres'].inside.rest = Prism.languages['spip-squelette']
	Prism.languages['spip-squelette']['spip-idiome'].inside['spip-parametres'].inside.rest = Prism.languages['spip-squelette']
	Prism.languages['spip-squelette']['spip-argument'].inside['parameter'].inside.rest = Prism.languages['spip-squelette']
	Prism.languages['spip-squelette']['spip-argument'].inside.rest = Prism.languages['spip-squelette']

}())